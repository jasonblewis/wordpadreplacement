program Wordpad;

{$APPTYPE CONSOLE}

uses
  IniFiles,
  Windows,
  SysUtils,
  FileUtil;

var
  appName: string = '';
  appOption: string = '';
  textfile, newfile: string;
  ini: TIniFile;

  function QuoteStr(const s: string): string;
  const
    QuoteChar = '"';
  begin
    Result := Concat(QuoteChar, s, QuoteChar);
  end;

  function RunProcess(const AppPath: string; MustWait: boolean = False;
    AppParams: string = ''; Visibility: word = SW_SHOWNORMAL): DWORD;
  var
    SI:      TStartupInfo;
    PI:      TProcessInformation;
    Proc:    THandle;
    zParams: array[0..256] of char;
  begin
    FillChar(SI, SizeOf(SI), 0);
    SI.cb := SizeOf(SI);
    SI.wShowWindow := Visibility;
    //  if not CreateProcess(StrPCopy(zFileName, AppPath), StrPCopy(zParams, AppParams),
    if not CreateProcess(nil,
      //lpApplicationName
      StrPCopy(zParams, AppPath + ' ' + AppParams),    //lpCommandLine
      nil,                                             //lpProcessAttributes
      nil,                                             //lpThreadAttributes
      False,                                           //bInheritedHandles
      Normal_Priority_Class or CREATE_NEW_CONSOLE or CREATE_NEW_PROCESS_GROUP,
      //dwCreationFlags
      nil,                                             //lpEnvironment
      nil,                                             //lpCurrentDirectory
      SI,                                              //lpStartupInfo
      PI)                                              //lpProcessInformation
    then
      raise Exception.CreateFmt('Failed to excecute program ' + AppPath +
        '. Error Code %d', [GetLastError]);

    Proc := PI.hProcess;
    CloseHandle(PI.hThread);

    if MustWait then
      if WaitForSingleObject(Proc, Infinite) <> Wait_Failed then
        GetExitCodeProcess(Proc, Result);

    CloseHandle(Proc);
  end;


{$IFDEF WINDOWS}{$R Wordpad.rc}{$ENDIF}

//{$R *.res}

begin
  // get the app name
  appName := 'C:\Program Files\Windows NT\Accessories\wordpad.exe';
  appOption := '';
  ini := TIniFile.Create(IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0))) +
    'config.ini');
  try
    try
      appName := ini.ReadString('Config', 'Exe', appName);
    finally
    end;
    try
      appOption := ini.ReadString('Config', 'option', appOption);
    finally
    end;
  finally
    FreeAndNil(ini);
  end;

  try
    if ParamCount < 1 then
      raise Exception.Create('no paramater given')
    else
    begin    // copy the file to a new file and open that before returning control to sirius - this is because sirius deletes the file before notepad can grab it
      textfile := ParamStr(1);
      //     newfile := textfile + '1';
      if not (FileExists(textfile)) then
        raise Exception.Create('text file missing')
      else
        newfile := GetTempFileName(GetTempDir(false), textfile);
      CopyFile(textfile, newfile);
    end;

    if FileExists(appName) then
    begin
      if appOption <> '' then
        newfile := appOption + ' ' + newfile;
      RunProcess(QuoteStr(appName), False, newfile);
    end
    else
        raise exception.Create('app does not exist:' + appName);


  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.

