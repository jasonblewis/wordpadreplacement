This project replaces wordpad with a shim that loads wordpad and returns control to the calling program ASAP.

Designed to work around programs that open wordpad (or any other program for that matter) and insist on blocking until wordpad closes.

I no longer need this so putting it here for posterity and in case anyone else needs something like this.

Works in Windows 64 bit.
